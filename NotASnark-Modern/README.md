# Modern Asset Pack for Dungeondraft

This is a set of assets for use with Dungeondraft. It isn't the asset pack itself, but the source files used to create the assets.

Source files are in xcf (GIMP) format, they need to be converted to png before building the pack.

For how to build an asset pack for Dungeondraft, see this page:

https://github.com/Megasploot/Dungeondraft/wiki/Custom-Assets-Guide


Some textures are taken from CC0 resources on these sites:
*  https://ambientcg.com/
*  https://polyhaven.com/textures

